module gitlab.com/slon/shad-go

go 1.13

require (
	github.com/ClickHouse/clickhouse-go v1.4.0
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/armon/consul-api v0.0.0-20180202201655-eb2c6b5be1b6 // indirect
	github.com/coreos/go-etcd v2.0.0+incompatible // indirect
	github.com/cpuguy83/go-md2man v1.0.10 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/go-resty/resty/v2 v2.1.0
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/golang/mock v1.4.1
	github.com/google/go-cmp v0.4.0
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/jackc/pgx/v4 v4.6.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/jonboulle/clockwork v0.1.0
	github.com/spf13/cobra v1.1.3
	github.com/stretchr/testify v1.5.1
	github.com/ugorji/go/codec v0.0.0-20181204163529-d75b2dcb6bc8 // indirect
	github.com/xordataexchange/crypt v0.0.3-0.20170626215501-b2862e3d0a77 // indirect
	go.uber.org/goleak v1.0.0
	go.uber.org/zap v1.14.0
	golang.org/x/net v0.0.0-20190813141303-74dc4d7220e7
	golang.org/x/perf v0.0.0-20191209155426-36b577b0eb03
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	golang.org/x/sys v0.0.0-20200409092240-59c9f1ba88fa
	golang.org/x/tools v0.0.0-20200125223703-d33eef8e6825
	gopkg.in/src-d/go-git.v4 v4.13.1 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
